package Demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWebShopElectronics {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		WebDriver driver=new ChromeDriver();
		
		driver.get("http://demowebshop.tricentis.com/login");
		Thread.sleep(2000);
		driver.findElement(By.id("Email")).sendKeys("pundlabhavya123@gmail.com");
		Thread.sleep(1000);
		driver.findElement(By.id("Password")).sendKeys("123456");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		
		WebElement electronics = driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));
		Actions act = new Actions(driver);
		act.moveToElement(electronics).build().perform();
		act.moveToElement(driver.findElement(By.partialLinkText("Cell phones"))).click().perform();
		
		Select sel = new Select(driver.findElement(By.id("products-orderby")));
		sel.selectByVisibleText("Price: Low to High");
		
		driver.findElement(By.xpath("(//input[@type='button'])[3]")).click();
		driver.findElement(By.xpath("//span[text()='Shopping cart']")).click();
		driver.findElement(By.xpath("(//input[@type='checkbox'])[2]")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		
		driver.findElement(By.id("BillingNewAddress_FirstName")).sendKeys("bhavya");
		driver.findElement(By.id("BillingNewAddress_LastName")).sendKeys("pundla");
		driver.findElement(By.id("BillingNewAddress_Email")).sendKeys("pundlabhavya123@gmail.com");
		
	}

}
