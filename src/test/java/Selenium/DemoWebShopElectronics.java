package Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWebShopElectronics {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys("pundlabhavya123@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("123456");
		driver.findElement(By.id("RememberMe")).click();
		driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
		WebElement findElement = driver
				.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Electronics')]"));

		Actions as = new Actions(driver);
		as.moveToElement(findElement).build().perform();
		as.moveToElement(driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Cell phones')]")))
				.click().perform();
		driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
		driver.findElement(By.partialLinkText("Shopping cart")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
		driver.findElement(By.id("BillingNewAddress_FirstName")).sendKeys("bhavya");
		driver.findElement(By.id("BillingNewAddress_LastName")).sendKeys("pundla");
		driver.findElement(By.id("BillingNewAddress_Email")).sendKeys("pundlabhavya123@gmail.com");
		driver.findElement(By.id("BillingNewAddress_City")).sendKeys("tpt");
		driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("tpt");
		driver.findElement(By.id("BillingNewAddress_Address2")).sendKeys("renigunta");
		driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("517520");
		driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("7995686139");
		driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
		
	}

}
