package Class2;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ImplicitWait {

	public static <Expected> void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
WebDriver driver=new ChromeDriver();
		
		driver.get("http://demowebshop.tricentis.com/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	
		driver.findElement(By.id("Email")).sendKeys("pundlabhavya123@gmail.com");
		
		driver.findElement(By.id("Password")).sendKeys("123456");

		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		
		String ttl1=driver.getTitle();
		
		String Expectedtitle="Demo Web Shop";
		
		if(ttl1.equalsIgnoreCase(Expectedtitle)) {
			System.out.println("Pass");
		}
		else
		{
			System.out.println("Fail");
		}
		
	}

}
